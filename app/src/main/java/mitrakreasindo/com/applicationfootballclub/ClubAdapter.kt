package com.dicoding.applicationfootbalclub

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import org.jetbrains.anko.AnkoContext

/**
 * Created by developer on 9/12/18.
 */
class ClubAdapter(private val clubs: List<Club>, private val listener : (Club) -> Unit)
    : RecyclerView.Adapter<ClubAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ClubUI().createView(AnkoContext.create(parent.context, parent)))
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(clubs[position], listener)
    }

    override fun getItemCount(): Int = clubs.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val img = itemView.findViewById<ImageView>(ClubUI.imageId)
        var txtTitle = itemView.findViewById<TextView>(ClubUI.nameId)

        fun bind(club: Club,  listener: (Club) -> Unit){
            txtTitle.text = club.clubName
            Glide.with(itemView.context).load(club.clubLogo).into(img)
            itemView.setOnClickListener{
                listener(club)
            }
        }


    }
}
