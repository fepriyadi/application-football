package com.dicoding.applicationfootbalclub

import android.view.Gravity
import android.view.ViewGroup
import android.widget.LinearLayout
import mitrakreasindo.com.applicationfootballclub.R
import org.jetbrains.anko.*

/**
 * Created by developer on 9/12/18.
 */


class ClubUI : AnkoComponent<ViewGroup> {

    companion object {
        val nameId = R.id.club_name
        val imageId = R.id.imgclub
    }

    override fun createView(ui: AnkoContext<ViewGroup>) = with(ui){
        linearLayout {
            orientation = LinearLayout.HORIZONTAL
            lparams(matchParent, wrapContent)
            padding = dip(16)

            imageView{
                id = imageId

            }.lparams(dip(50), dip(50))

            textView {
                id = nameId
            }.lparams(matchParent, wrapContent){
                margin = dip(10)
                gravity = Gravity.CENTER_VERTICAL
            }

        }
    }
}

