package com.dicoding.applicationfootbalclub

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.widget.LinearLayout
import mitrakreasindo.com.applicationfootballclub.R
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView

class MainActivity : AppCompatActivity() {
    private var items: MutableList<Club> = mutableListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initData()
        val adapter = ClubAdapter(items){
            startActivity<DetailActivity>(DetailActivity.POSITIONEXTRA to it)
            toast("${it.clubName}")
        }
        MainActivityUI(adapter).setContentView(this)
    }

    class MainActivityUI(val mAdapter:ClubAdapter) : AnkoComponent<MainActivity> {
        override fun createView(ui: AnkoContext<MainActivity>) = with(ui) {
            verticalLayout {
                lparams(matchParent, matchParent)
                orientation = LinearLayout.VERTICAL

                recyclerView {
                    lparams(matchParent, matchParent)
                    layoutManager = LinearLayoutManager(context)
                    adapter = mAdapter
                    }
                }
            }
        }

    private fun initData(){
        val name = resources.getStringArray(R.array.club_name)
        val image = resources.obtainTypedArray(R.array.club_image)
        val description = resources.getStringArray(R.array.club_description)
        items.clear()
        for (i in name.indices) {
            items.add(Club(image.getResourceId(i, 0), name[i], description[i]))
        }

        //Recycle the typed array
        image.recycle()
    }
}
