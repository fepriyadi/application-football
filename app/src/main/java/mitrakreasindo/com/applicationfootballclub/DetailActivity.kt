package com.dicoding.applicationfootbalclub

import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import org.jetbrains.anko.*

/**
 * Created by developer on 9/12/18.
 */


class DetailActivity : AppCompatActivity() {

    companion object {
        val keteranganID = 3
        val POSITIONEXTRA = "position_extra"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val intent = intent
        val list = intent.getParcelableExtra<Club>(POSITIONEXTRA)

        DetailActivityUI(list).setContentView(this)

    }

    internal class DetailActivityUI(var list: Club) : AnkoComponent<DetailActivity> {

        @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
        override fun createView(ui: AnkoContext<DetailActivity>) = with(ui){
            var position = 0
            linearLayout {
                orientation = LinearLayout.VERTICAL
                lparams(matchParent, matchParent)

                imageView(){
                    Glide.with(this).load(list.clubLogo).into(this)
                    id = ClubUI.imageId
                    padding = dip(10)
                    this@linearLayout.gravity = Gravity.CENTER_HORIZONTAL
                }.lparams(dip(80), dip(80))

                textView{
                    id = ClubUI.nameId
                    text = list.clubName
                    textSize = sp(5).toFloat()
                    gravity = Gravity.CENTER_HORIZONTAL
                    bottomPadding = dip(10)
                }

                textView{
                    id = keteranganID
                    text = list.clubDesription
                    padding = dip(10)
                }

            }
        }

    }
}

