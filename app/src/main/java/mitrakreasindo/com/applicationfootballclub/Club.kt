package com.dicoding.applicationfootbalclub

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by developer on 9/12/18.
 */
@Parcelize
data class Club(val clubLogo: Int?, val clubName: String?, val clubDesription :String) : Parcelable